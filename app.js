const express = require('express');
const mongoose = require('mongoose');
require('dotenv/config');
const app = express();

//Routes
const postsRoute = require('./routes/posts');


//middleswares
app.use('/somewhere', postsRoute);



mongoose.connect(
    process.env.BDD_URL,
    { useNewUrlParser: true },
    () => console.log('BDD connectée!')
);

//app listener
app.listen(3000);